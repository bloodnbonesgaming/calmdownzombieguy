package com.bloodnbonesgaming.calmdownzombieguy;

public class ModInfo {
	public static final String MODID = "calmdownzombieguy";
	public static final String MOD_NAME = "Calm Down Zombie Guy";
	public static final String VERSION = "@VERSION@";
	public static final String CONFIG_FOLDER = "./config/calmdownzombieguy/";
    public static final String MAIN_CONFIG_FILE = ModInfo.CONFIG_FOLDER + "CalmDownZombieGuy.txt";
}
