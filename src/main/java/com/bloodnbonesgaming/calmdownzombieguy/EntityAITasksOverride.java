package com.bloodnbonesgaming.calmdownzombieguy;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import net.minecraft.entity.ai.EntityAITasks;

public class EntityAITasksOverride extends EntityAITasks {
	
	private static final Random rand = new Random();
	public static final Map<Class<?>, Integer> taskSkipChances = new HashMap<Class<?>, Integer>();

	public EntityAITasksOverride(EntityAITasks original) {
		super(original.profiler);
		this.tickRate = original.tickRate;
		this.taskEntries.addAll(original.taskEntries);
		this.executingTaskEntries.addAll(original.executingTaskEntries);
		this.tickCount = original.tickCount;
		this.disabledControlFlags = original.disabledControlFlags;
	}
	
	@Override
	public void onUpdateTasks() {
		//If there's nothing to skip, just use the vanilla version
		if (taskSkipChances.isEmpty()) {
			super.onUpdateTasks();
			return;
		}
		
		if (this.tickCount++ % this.tickRate == 0)
        {
            for (EntityAITasks.EntityAITaskEntry entityaitasks$entityaitaskentry : this.taskEntries)
            {
            	if (!this.shouldTickTask(entityaitasks$entityaitaskentry)) {
            		continue;
            	}
                if (entityaitasks$entityaitaskentry.using)
                {
                    if (!this.canUse(entityaitasks$entityaitaskentry) || !this.canContinue(entityaitasks$entityaitaskentry))
                    {
                        entityaitasks$entityaitaskentry.using = false;
                        entityaitasks$entityaitaskentry.action.resetTask();
                        this.executingTaskEntries.remove(entityaitasks$entityaitaskentry);
                    }
                }
                else if (this.canUse(entityaitasks$entityaitaskentry) && entityaitasks$entityaitaskentry.action.shouldExecute())
                {
                    entityaitasks$entityaitaskentry.using = true;
                    entityaitasks$entityaitaskentry.action.startExecuting();
                    this.executingTaskEntries.add(entityaitasks$entityaitaskentry);
                }
            }
        }
        else
        {
            Iterator<EntityAITasks.EntityAITaskEntry> iterator = this.executingTaskEntries.iterator();

            while (iterator.hasNext())
            {
                EntityAITasks.EntityAITaskEntry entityaitasks$entityaitaskentry1 = iterator.next();
                
            	if (!this.shouldTickTask(entityaitasks$entityaitaskentry1)) {
            		continue;
            	}

                if (!this.canContinue(entityaitasks$entityaitaskentry1))
                {
                    entityaitasks$entityaitaskentry1.using = false;
                    entityaitasks$entityaitaskentry1.action.resetTask();
                    iterator.remove();
                }
            }
        }

        if (!this.executingTaskEntries.isEmpty())
        {
            for (EntityAITasks.EntityAITaskEntry entityaitasks$entityaitaskentry2 : this.executingTaskEntries)
            {
            	if (!this.shouldTickTask(entityaitasks$entityaitaskentry2)) {
            		continue;
            	}
                entityaitasks$entityaitaskentry2.action.updateTask();
            }
        }
	}
	
	private boolean shouldTickTask(EntityAITasks.EntityAITaskEntry taskEntry) {
		//Try to find an entry which matches the task
		for (Entry<Class<?>, Integer> entry : taskSkipChances.entrySet()) {
			if (taskEntry.action.getClass().isAssignableFrom(entry.getKey())) {
				//Roll 100. If chance to tick is higher than roll, allow it to tick
				if (entry.getValue() > 0 && entry.getValue() > rand.nextInt(100)) {
					return true;
				}
				return false;
			}
		}
		//Let it tick if it's not in the list
		return true;
	}
}
