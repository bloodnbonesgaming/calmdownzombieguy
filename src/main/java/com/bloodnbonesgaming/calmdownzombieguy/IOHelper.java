package com.bloodnbonesgaming.calmdownzombieguy;

import java.io.File;
import java.util.Map;

import com.bloodnbonesgaming.lib.util.FileHelper;
import com.bloodnbonesgaming.lib.util.script.ScriptUtil;

public class IOHelper {
	
    public static void readMainFile(final MainConfig config, final Map<String, Class> classKeywords)
    {
        final File file = new File(ModInfo.MAIN_CONFIG_FILE);

        if (!file.exists())
        {
            FileHelper.copyDirectoryFromJar(IOHelper.class, "/defaultconfigs/", ModInfo.CONFIG_FOLDER);
        }
        ScriptUtil.readScript(file, config, classKeywords);
    }
}
