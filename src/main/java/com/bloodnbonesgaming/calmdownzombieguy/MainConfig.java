package com.bloodnbonesgaming.calmdownzombieguy;

public class MainConfig {
	
	private int maxSpawnChunkDistanceAroundPlayer = 8;
	private int minSpawnChunkDistanceAroundPlayer = 0;
	private int minSpawnDistanceAroundPlayer = 24;
	private int maxSpawnDistanceAroundPlayer = 0;
	private int newTaskTickRate = 3;
	private int minWaitBetweenZombieAttackAttempts = 5;
	private int maxWaitBetweenZombieAttackAttempts = 15;
	private boolean reduceAllowedSpawnAreaChecks = true;
	private boolean ignoreEmptyChunks = true;
	private int entityWanderChance = 120;
	private double entityAttackTargetMultiplier = 1;
	private int entityLivingMaxUpdateDistanceFromPlayer = (int) Math.pow(96, 2); //Square the distance so we can use a cheaper calculation - Probably never going to use this, it's bad
	private int maxTickTimeBeforeCancellingEntityUpdates = 30;
	
	public void setMinWaitBetweenZombieAttackAttempts(final int ticks)
	{
		this.minWaitBetweenZombieAttackAttempts = ticks;
	}
	
	public int getMinWaitBetweenZombieAttackAttempts()
	{
		return this.minWaitBetweenZombieAttackAttempts;
	}
	
	public void setMaxWaitBetweenZombieAttackAttempts(final int ticks)
	{
		this.maxWaitBetweenZombieAttackAttempts = ticks;
	}
	
	public int getMaxWaitBetweenZombieAttackAttempts()
	{
		return this.maxWaitBetweenZombieAttackAttempts;
	}
	
	public void setMaxSpawnChunkDistanceAroundPlayer(final int distance)
	{
		this.maxSpawnChunkDistanceAroundPlayer = distance;
	}
	
	public int getMaxSpawnChunkDistanceAroundPlayer()
	{
		return this.maxSpawnChunkDistanceAroundPlayer;
	}
	
	public void setMinSpawnChunkDistanceAroundPlayer(final int distance)
	{
		this.minSpawnChunkDistanceAroundPlayer = distance;
	}
	
	public int getMinSpawnChunkDistanceAroundPlayer()
	{
		return this.minSpawnChunkDistanceAroundPlayer;
	}
	
	public void setMaxSpawnDistanceAroundPlayer(final int distance)
	{
		this.maxSpawnDistanceAroundPlayer = distance;
	}
	
	public int getMaxSpawnDistanceAroundPlayer()
	{
		return this.maxSpawnDistanceAroundPlayer;
	}
	
	public void setMinSpawnDistanceAroundPlayer(final int distance)
	{
		this.minSpawnDistanceAroundPlayer = distance;
	}
	
	public int getMinSpawnDistanceAroundPlayer()
	{
		return this.minSpawnDistanceAroundPlayer;
	}
	
	public void setNewTaskTickRate(final int rate)
	{
		this.newTaskTickRate = rate;
	}
	
	public int getNewTaskTickRate()
	{
		return this.newTaskTickRate;
	}
	
	public void reduceAllowedSpawnAreaChecks(final boolean bool)
	{
		this.reduceAllowedSpawnAreaChecks = bool;
	}
	
	public boolean getReduceAllowedSpawnAreaChecks()
	{
		return this.reduceAllowedSpawnAreaChecks;
	}
	
	public void ignoreEmptyChunks(final boolean bool)
	{
		this.ignoreEmptyChunks = bool;
	}
	
	public boolean shouldIgnoreEmptyChunks()
	{
		return this.ignoreEmptyChunks;
	}

	public int getEntityWanderChance() {
		return entityWanderChance;
	}

	public void setEntityWanderChance(int entityWanderChance) {
		this.entityWanderChance = entityWanderChance;
	}

	public double getEntityAttackTargetMultiplier() {
		return entityAttackTargetMultiplier;
	}

	public void setEntityAttackTargetMultiplier(double entityAttackTargetMultiplier) {
		this.entityAttackTargetMultiplier = entityAttackTargetMultiplier;
	}
	
	public void setTickChanceForTask(String task, int chance) throws ClassNotFoundException {
		EntityAITasksOverride.taskSkipChances.put(Class.forName(task), chance);
	}

	public int getEntityLivingMaxUpdateDistanceFromPlayer() {
		return entityLivingMaxUpdateDistanceFromPlayer;
	}

	public void setEntityLivingMaxUpdateDistanceFromPlayer(int entityLivingMaxUpdateDistanceFromPlayer) {
		this.entityLivingMaxUpdateDistanceFromPlayer = entityLivingMaxUpdateDistanceFromPlayer * entityLivingMaxUpdateDistanceFromPlayer;
	}

	public int getMaxTickTimeBeforeCancellingEntityUpdates() {
		return maxTickTimeBeforeCancellingEntityUpdates;
	}

	public void setMaxTickTimeBeforeCancellingEntityUpdates(int maxTickTimeBeforeCancellingEntityUpdates) {
		this.maxTickTimeBeforeCancellingEntityUpdates = maxTickTimeBeforeCancellingEntityUpdates;
	}
}
