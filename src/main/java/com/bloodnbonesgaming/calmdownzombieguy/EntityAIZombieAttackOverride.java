package com.bloodnbonesgaming.calmdownzombieguy;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIZombieAttack;
import net.minecraft.entity.monster.EntityZombie;

public class EntityAIZombieAttackOverride extends EntityAIZombieAttack {

	public EntityAIZombieAttackOverride(EntityZombie zombieIn, double speedIn, boolean longMemoryIn) {
		super(zombieIn, speedIn, longMemoryIn);
	}
	
	@Override
	public boolean shouldExecute() {
		EntityLivingBase entitylivingbase = this.attacker.getAttackTarget();

        if (entitylivingbase == null)
        {
        	return false;
        }
        else if (!entitylivingbase.isEntityAlive())
        {
        	return false;
        }
        else if (!this.sameDimension(entitylivingbase))
        {
        	return false;
        }
        else
        {
        	this.path = this.attacker.getNavigator().getPathToEntityLiving(entitylivingbase);
            
            if (path != null)
            {
            	return true;
            }
//            final MainConfig config = ConfigurationManager.getConfig();
//            final double distance = this.attacker.getDistanceSq(entitylivingbase.posX, entitylivingbase.getEntityBoundingBox().minY, entitylivingbase.posZ);
//            this.delayCounter = (int) (Math.min(1.0, distance / 16384) * (config.getMaxWaitBetweenZombieAttackAttempts() - config.getMinWaitBetweenZombieAttackAttempts()) + config.getMinWaitBetweenZombieAttackAttempts());
        }
        return this.getAttackReachSqr(entitylivingbase) >= this.attacker.getDistanceSq(entitylivingbase.posX, entitylivingbase.getEntityBoundingBox().minY, entitylivingbase.posZ);
	}
	
	@Override
	public boolean shouldContinueExecuting() {
		EntityLivingBase entitylivingbase = this.attacker.getAttackTarget();

        if (entitylivingbase == null)
        {
            return false;
        }
        else if (!entitylivingbase.isEntityAlive())
        {
            return false;
        }
        else if (!this.sameDimension(entitylivingbase))
        {
        	return false;
        }
		return super.shouldContinueExecuting();
	}
	
	private boolean sameDimension(EntityLivingBase target)
	{
		return this.attacker.world.provider.getDimension() == target.world.provider.getDimension();
	}
}
