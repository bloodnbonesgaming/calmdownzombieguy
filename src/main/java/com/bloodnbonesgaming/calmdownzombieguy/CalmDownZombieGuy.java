package com.bloodnbonesgaming.calmdownzombieguy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.Logger;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIZombieAttack;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ServerTickEvent;

@Mod(modid = ModInfo.MODID, name = ModInfo.MOD_NAME, version = ModInfo.VERSION, acceptedMinecraftVersions = "[1.12,1.13)", acceptableRemoteVersions = "*", dependencies = "required-after:bnbgaminglib@[2.14.0,);")
public class CalmDownZombieGuy {
	
	@Instance(ModInfo.MODID)
	public static CalmDownZombieGuy instance;
		
	private Logger log;
	
	@EventHandler
	public void preInit(final FMLPreInitializationEvent event)
	{
		this.log = event.getModLog();
		MinecraftForge.EVENT_BUS.register(this);
		ConfigurationManager.read();
	}
	
	@SubscribeEvent
	public void onEntityJoinWorld(final EntityJoinWorldEvent event)
	{
		if (!event.getEntity().world.isRemote)
		{
			if (event.getEntity() instanceof EntityZombie) {
				
				List<EntityAITaskEntry> toAdd = new ArrayList<EntityAITaskEntry>();
				EntityZombie zombie = (EntityZombie) event.getEntity();
				
				Iterator<EntityAITaskEntry> iterator = zombie.tasks.taskEntries.iterator();
				
				while (iterator.hasNext())
				{
					EntityAITaskEntry entry = iterator.next();
					
					if (entry.action instanceof EntityAIZombieAttack && !(entry.action instanceof EntityAIZombieAttackOverride))
					{
						EntityAIZombieAttack action = (EntityAIZombieAttack) entry.action;
						toAdd.add(zombie.tasks.new EntityAITaskEntry(entry.priority, new EntityAIZombieAttackOverride(zombie, action.speedTowardsTarget, action.longMemory)));
						iterator.remove();
					}
				}
				zombie.tasks.taskEntries.addAll(toAdd);
			}
			if (event.getEntity() instanceof EntityLiving)
			{
				EntityLiving living = (EntityLiving) event.getEntity();
				
				//Only replace the tasks objects if they're vanilla
				if (living.tasks.getClass() == EntityAITasks.class) {
					living.tasks = new EntityAITasksOverride(living.tasks);
				}
				if (living.targetTasks.getClass() == EntityAITasks.class) {
					living.targetTasks = new EntityAITasksOverride(living.targetTasks);
				}
				
				final int tickrate = ConfigurationManager.getConfig().getNewTaskTickRate();
				living.tasks.tickRate = tickrate;
				living.targetTasks.tickRate = tickrate;
								
				Iterator<EntityAITaskEntry> iterator = living.tasks.taskEntries.iterator();
				
				while (iterator.hasNext())
				{
					EntityAITaskEntry entry = iterator.next();
					
					if (entry.action instanceof EntityAIWander)
					{
						((EntityAIWander)entry.action).executionChance = ConfigurationManager.getConfig().getEntityWanderChance();
					}
					if (entry.action instanceof EntityAINearestAttackableTarget)
					{
						((EntityAINearestAttackableTarget<?>) entry.action).targetChance = (int) Math
								.ceil(((EntityAINearestAttackableTarget<?>) entry.action).targetChance
										* ConfigurationManager.getConfig().getEntityAttackTargetMultiplier());
					}
				}

				iterator = living.targetTasks.taskEntries.iterator();

				while (iterator.hasNext()) {
					EntityAITaskEntry entry = iterator.next();

					if (entry.action instanceof EntityAIWander) {
						((EntityAIWander) entry.action).executionChance = ConfigurationManager.getConfig()
								.getEntityWanderChance();
					}
					if (entry.action instanceof EntityAINearestAttackableTarget) {
						((EntityAINearestAttackableTarget<?>) entry.action).targetChance = (int) Math
								.ceil(((EntityAINearestAttackableTarget<?>) entry.action).targetChance
										* ConfigurationManager.getConfig().getEntityAttackTargetMultiplier());
					}
				}
			}
		}
	}
	
	@SubscribeEvent
	public void onLivingUpdate(LivingUpdateEvent event) {
		if (!event.getEntityLiving().world.isRemote) {
			//Let through entities in other dimensions, since I can't think of a way to handle them better yet
			if (event.getEntityLiving().world.provider.getDimension() != 0) {
				return;
			}
			//Let through players, dead entities, entities being ridden by players and bosses
			if (event.getEntityLiving() instanceof EntityPlayer || event.getEntityLiving().isDead
					|| event.getEntityLiving().getRidingEntity() instanceof EntityPlayer
					|| event.getEntityLiving() instanceof EntityWither
					|| event.getEntityLiving() instanceof EntityDragon) {
				tickedEntities++;
				return;
			}
			//Allow through all entities attacking players
			if (event.getEntityLiving() instanceof EntityLiving && ((EntityLiving) event.getEntityLiving()).getAttackTarget() instanceof EntityPlayer) {
				tickedEntities++;
				return;
			}
			if (System.currentTimeMillis() - tickStartTime > ConfigurationManager.getConfig().getMaxTickTimeBeforeCancellingEntityUpdates()) {
				skippedEntities++;
				event.setCanceled(true);
				return;
			}
			tickedEntities++;
		}
	}
	
	private boolean sorted = false;
	private long tickStartTime = 0;
	private int skippedEntities = 0;
	private int tickedEntities = 0;
	
	@SubscribeEvent
	public void onPlayerUpdate(PlayerTickEvent event) {
		if (!event.player.world.isRemote) {
			if (!sorted && event.phase == TickEvent.Phase.START) {
				//Sort loaded entity list
				World overworld = DimensionManager.getWorld(0);
				
				if (overworld != null) {
					overworld.loadedEntityList.sort(EntityDistanceComparator.INSTANCE);
					sorted = true;
				}
			}
		}
	}
	
	@SubscribeEvent
	public void onServerTick(ServerTickEvent event) {
		if (event.phase == TickEvent.Phase.START) {
			sorted = false;
			skippedEntities = 0;
			tickedEntities = 0;
			tickStartTime = System.currentTimeMillis();
//			CalmDownZombieGuy.instance.getLog().info("Tick starting");
		} else if (event.phase == TickEvent.Phase.END && skippedEntities > 0) {
//			CalmDownZombieGuy.instance.getLog().info(skippedEntities + "/" + tickedEntities + " entities skipped/ticked");
		}
	}

	public Logger getLog() {
		return this.log;
	}
	
	@EventHandler
	public void onServerAboutToStart(final FMLServerAboutToStartEvent event)
	{
		ConfigurationManager.read();
	}
	
	private final Spawner spawner = new Spawner();
	
//	@SubscribeEvent
//	public void onMobSpawning(final MobSpawningEvent event)
//	{
//		this.spawner.findChunksForSpawning(event.server, event.spawnHostileMobs, event.spawnPeacefulMobs, event.spawnOnSetTickRate);
//		event.setCanceled(true);
//	}
}
