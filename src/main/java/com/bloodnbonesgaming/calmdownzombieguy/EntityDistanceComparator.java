package com.bloodnbonesgaming.calmdownzombieguy;

import java.util.Comparator;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;

public class EntityDistanceComparator implements Comparator<Entity>{
	
	public static EntityDistanceComparator INSTANCE = new EntityDistanceComparator();

	@Override
	public int compare(Entity entity1, Entity entity2) {

		if (entity1 instanceof EntityPlayer && entity2 instanceof EntityPlayer) {
			return 0; //Sort same
		}
		if (entity1 instanceof EntityPlayer) {
			return 1; //Sort 1 above 2 - Players go above everything
		}
		if (entity2 instanceof EntityPlayer) {
			return -1; //Sort 2 above 1 - Players go above everything
		}
		if (entity1.isDead && entity2.isDead) {
			return 0; //Sort same
		}
		if (entity1.isDead) {
			return 1; //Sort 1 above 2 - Dead entities go above anything but players
		}
		if (entity2.isDead) {
			return -1; //Sort 2 above 1 - Dead entities go above anything but players
		}
		if (entity1 instanceof EntityLivingBase && !(entity2 instanceof EntityLivingBase)) {
			return -1; //Sort 2 above 1 - Only living entities can be canceled, so just put other entities ahead to get a better idea on tick time usage
		}
		if (entity2 instanceof EntityLivingBase && !(entity1 instanceof EntityLivingBase)) {
			return 1; //Sort 1 above 2 - Only living entities can be canceled, so just put other entities ahead to get a better idea on tick time usage
		}
		double closestPlayer1 = Double.MAX_VALUE;
		double closestPlayer2 = Double.MAX_VALUE;
		
		for (EntityPlayer player : entity1.world.playerEntities) {
			double distance1 = player.getDistanceSq(entity1);
			if (distance1 < closestPlayer1) {
				closestPlayer1 = distance1;
			}
			double distance2 = player.getDistanceSq(entity2);
			if (distance2 < closestPlayer2) {
				closestPlayer2 = distance2;
			}
		}
		if (closestPlayer1 == closestPlayer2) {
			return 0; //Sort same if both are the same distance from closest player. Should only happen if there are no players.
		}
		return closestPlayer1 > closestPlayer2 ? 1 : -1;//Sort based on distance
	}

}
