package com.bloodnbonesgaming.calmdownzombieguy;

public class ConfigurationManager {
	
	private static MainConfig config = new MainConfig();
	
	
	public static void read()
	{
		ConfigurationManager.clean();
		ConfigurationManager.config = new MainConfig();
		IOHelper.readMainFile(ConfigurationManager.config, null);
	}
	
	public static void clean()
	{
		ConfigurationManager.config = null;
	}
	
	public static MainConfig getConfig()
	{
		if (ConfigurationManager.config == null)
		{
			ConfigurationManager.read();
		}
		return ConfigurationManager.config;
	}
}
